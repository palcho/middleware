#ifndef MIDDLEWARE_HEADER
#define MIDDLEWARE_HEADER

typedef enum {
	BECOME_PUBLISHER = 0x01234567, // c->s create a subscription
	JOIN_PUBLISHER   = 0x134ab152, // c->s join a publisher in a subscription
	PUBLISH          = 0xd13ff149, // c->s publish an update to a subscription
	SUBSCRIBE        = 0x98ba1084, // c->s subscribe to one subscription
	CHECK_INBOX      = 0x14813421, // c->s request updates from a subscription
	LIST             = 0x89711aaf, // c->s list all subscriptions
	UPDATES          = 0xf1341887, // s->c subscriptions updates received
	OK               = 0xa98df790, // s->c what you asked for is done
} Header;

typedef struct Publisher {
	uint32_t num_publishers;
	uint32_t data_length;
	char publishers[16][2];
} Publisher;

/***
 * a string that contains the last error message. print it if you want
 * default message: "MW: no error\n"
 **/
extern char mw_error_string[];
extern char inward_buffer[]; 
extern Publisher publishers[];

/***
 * serialize data to be set away to the network
 * the format of the data structure is indicated in the string fmt 
 * b = a byte, w = a word, d = a double word, q = a quad word, p = a pointer
 * each letter may be prefixed by a counter. ex:"2pd4w"
 * the function returns the size of the structure (the same of sizeof())
 * a short count indicates an error and mw_erro_string is updated, or
 * an ircorrect formatted string.
 *
 * TODO: 32-bit pointers and parenthesis for block repetition
 * TODO: update the comment above. it's outdated
 **/ 
int mw_serialize (void *data, const char *fmt);


/***
 * use this function before using any other function from the middleware
 * returns 0 on error: check mw_error_string for more information on the error
 **/
int mw_server_run ();

// functions to be done (server side)
int mw_server_new_publisher (int storage_expiration);
int mw_server_new_subscriber ();
int mw_server_join_publishers (); // check if publisher accepts
int mw_server_remove_publisher ();
int mw_server_remove_subscriber ();
	// low priority
	int mw_server_list_publishers ();
	int mw_server_list_subscribers (); // from a publisher
	int mw_server_list_messages (); // from a publisher queue
	int mw_server_list_subscriptions (); // from a subscriber
int mw_server_quit ();

// functions to be done (client side)
int mw_connect_to_server (const char *host);
int mw_become_publisher (const char *name); // returns the id 
int mw_join_publisher (int id, const char *name); // returns the input if all right
int mw_publish (int publisher_id, void *data, int size); // return id if all right
int mw_list_publishers (); // returns number of subscriptions data is at publishers (array of struct Publisher)
int mw_subscribe (); // returns size of data in bytes; and data is at inward_buffer
int mw_check_inbox (); // returns size of data in bytes; and data is at inward_buffer
int mw_quit ();

#endif // MIDDLEWARE_HEADER
