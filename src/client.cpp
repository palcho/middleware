#include <iostream>
#include <string>
#include <vector>

std::vector<std::string> separate(const std::string &line) {
	std::vector<std::string> ans;
	std::string str;
	for(auto c : line) {
		if(c == ' ') {
			if(!str.empty()) {
				ans.push_back(str);
				str.clear();
			}
		} else {
			str += c;
		}
	}
	if(!str.empty()) {
		ans.push_back(str);
	}
	return ans;
}

void matchList() {

}

void matchSpectate(const std::string &id) {

}

void playerList() {

}

void playerChallenge(const std::string &id) {

}

void makeMove(const std::string &id) {

}

void run() {
	// help
	std::string str;
	while(std::getline(std::cin, str)) {
		auto tokens = separate(str);
		if(tokens.size() == 1 && tokens[0] == "help") {
			// print text
		} else if(tokens.size() == 2 && tokens[0] == "match" && tokens[1] == "list") {
			// get list of matches in server
			matchList();
		} else if(tokens.size() == 3 && tokens[0] == "match" && tokens[1] == "spectate") {
			// spectates match
			matchSpectate(tokens[2]);
		} else if(tokens.size() == 2 && tokens[0] == "player" && tokens[1] == "list") {
			// get list of players in server
			playerList();
		} else if(tokens.size() == 2 && tokens[0] == "challenge") {
			// challenge other player
			playerChallenge(tokens[1]);
		} else if(tokens.size() == 1 && tokens[0] == "ongoing") {
			// prints ongoing matches
		} else if(tokens.size() == 2 && tokens[0] == "move") {
			// tries to make a move in some game
			makeMove(tokens[1]);
		} else {
			std::cout << "invalid command" << std::endl;
		}
	}
}

int main() {
	run();
}