#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <stdbool.h>
#include <string.h>
#include "SDL2/SDL.h"
#include "SDL2/SDL_net.h"
#include "middleware.h"

#define set_error(...) do { sprintf(mw_error_string, "MW error: " __VA_ARGS__); return 0; } while (0)
enum { INBOUND, OUTBOUND };

char mw_error_string[128] = "MW: no error";

///////////////////// TEST BEGIN /////////////////////////////
struct Boi {
	int a, b;
	char c;
	char *p;
	short k, d;
	char l;
};


int main (int argc, char **argv) {
	struct Boi boi = { .a = 0x01020304, .b = 0x05060708, .c = 0x09, .p = (char *)0xfedcba9876543210, .k = 0x1122, .d = 0x3344, .l = 0xaa };
	int result;
	printf("a=0x%x, b=0x%x, c=0x%x, p=%p, k=0x%x, d=0x%x, l=0x%x\n", boi.a, boi.b, boi.c, boi.p, boi.k, boi.d, boi.l);
	result = mw_serialize(&boi, ">2dbp2wb");
	printf("result = %d, sizeof(boi) = %"PRIu64" -- %s\n", result, sizeof(boi), mw_error_string);
	printf("a=0x%x, b=0x%x, c=0x%x, p=%p, k=0x%x, d=0x%x, l=0x%x\n", boi.a, boi.b, boi.c, boi.p, boi.k, boi.d, boi.l);
	result = mw_serialize(&boi, "<2dbp2wb");
	printf("result = %d, sizeof(boi) = %"PRIu64" -- %s\n", result, sizeof(boi), mw_error_string);
	printf("a=0x%x, b=0x%x, c=0x%x, p=%p, k=0x%x, d=0x%x, l=0x%x\n", boi.a, boi.b, boi.c, boi.p, boi.k, boi.d, boi.l);
	//mw_server_run();

	
#define err(id_) fprintf(stderr, "%d: %s\n", id_, mw_error_string)
	result = mw_connect_to_server("127.0.0.1");
	err(result);
	result = mw_list_publishers();
	err(result);
	result = mw_become_publisher("moi");
	err(result);
	result = mw_list_publishers();
	err(result);
	result = mw_join_publisher(0);
	err(result);
	
	

	//printf("%s\n", mw_error_string);
	return 0;
}
////////////////////// TEST END //////////////////////////////

#define SERVER_PORT 54321
#define BUFFER_SIZE 512
static TCPsocket listening_socket, socket;
static IPaddress local_address, server_address;
static SDL_Thread *listen_thread, *thread;
static int num_clients = 0;

static int listen_network (void *data);
static int serve_client (void *data);
static int talk_to_server (void *data);

int mw_server_run () {
	int result;

	result = SDLNet_Init();
	if (result != 0) set_error("failed to initialize SDL2_net: %s", SDLNet_GetError());

	result = SDLNet_ResolveHost(&local_address, NULL, SERVER_PORT);
	if (result != 0) set_error("failed to resolve host: %s", SDLNet_GetError());
	printf("host = %u, port = %u\n", local_address.host, SDL_SwapBE16(local_address.port));

	listening_socket = SDLNet_TCP_Open(&local_address);
	if (!listening_socket) set_error("failed to open socket: %s", SDLNet_GetError());

	num_clients = 0;
	//listen_thread = SDL_CreateThread(listen_network, NULL, NULL);
	//if (!listen_thread) set_error("failed to create listening thread: %s", SDL_GetError());

	SDL_Thread *client_thread;
	TCPsocket client_socket;
	SDL_Event event;
	bool quit = false;

	result = SDL_Init(SDL_INIT_EVENTS);
	if (result != 0) set_error("failed to initialize events system: %s", SDL_GetError());

	while (!quit) {
		client_socket = SDLNet_TCP_Accept(listening_socket);
		if (client_socket) {
			client_thread = SDL_CreateThread(serve_client, NULL, client_socket);
			SDL_DetachThread(client_thread);
		}
		SDL_Delay(30);

		while (SDL_PollEvent(&event)) {
			if (event.type == SDL_QUIT) quit = true;
		}
	}

	SDLNet_TCP_Close(listening_socket);
	SDLNet_Quit();
	SDL_Quit(); 

	return 1;
}

int mw_connect_to_server (const char *host) {
	int result;

	result = SDLNet_Init();
	if (result != 0) set_error("failed to initialize SDL2_net: %s", SDLNet_GetError());

	result = SDLNet_ResolveHost(&server_address, host, SERVER_PORT);
	if (result != 0) set_error("failed to resolve host: %s", SDLNet_GetError());

	socket = SDLNet_TCP_Open(&server_address);
	if (!socket) set_error("failed to open socket: %s", SDLNet_GetError());

	//thread = SDL_CreateThread(talk_to_server, NULL, NULL);
	//SDL_DetachThread(thread);

	return 1;
}

typedef struct Formulary {
	uint32_t header, id;
} Formulary;
char client_buffer[BUFFER_SIZE];

int mw_become_publisher (const char *name) {
	int count;
	uint32_t *header = (uint32_t *)client_buffer;
	uint32_t *id = (uint32_t *)(client_buffer + sizeof(uint32_t));

	printf("become publisher\n");
	*header = SDL_SwapBE32(BECOME_PUBLISHER);
	printf("header = 0x%x\n", *header);
	strcpy(client_buffer + sizeof(uint32_t), name);
	printf("header = 0x%x\n", *header);
	count = SDLNet_TCP_Send(socket, client_buffer, 20);
	printf("become publisher\n");
	count = SDLNet_TCP_Recv(socket, client_buffer, BUFFER_SIZE); 
	printf("become publisher\n");
	return SDL_SwapBE32(*id);
}

int mw_join_publisher (int publisher_id, const char *name) {
	int count;
	uint32_t *header = (uint32_t *)client_buffer;
	uint32_t *id = (uint32_t *)(client_buffer + sizeof(uint32_t));

	*header = SDL_SwapBE32(JOIN_PUBLISHER);
	*id = SDL_SwapBE32(publisher_id);
	strcpy(client_buffer + sizeof(Formulary), name);
	count = SDLNet_TCP_Send(socket, client_buffer, 24);
	count = SDLNet_TCP_Recv(socket, client_buffer, BUFFER_SIZE); 
	return SDL_SwapBE32(*id);
}

int mw_publish (int publisher_id, void *data, int size) {
	int count;
	uint32_t *header = (uint32_t *)client_buffer;
	uint32_t *id = (uint32_t *)(client_buffer + sizeof(uint32_t));

	*header = SDL_SwapBE32(PUBLISH);
	*id = SDL_SwapBE32(publisher_id);
	memcpy(client_buffer+8, data, size);
	count = SDLNet_TCP_Send(socket, client_buffer, size+8);
	count = SDLNet_TCP_Recv(socket, client_buffer, BUFFER_SIZE); 
	return *id;
} 

#define MAX_SUBSCRIPTIONS 128

Publisher publishers[MAX_SUBSCRIPTIONS];

int mw_list_publishers () {
	int count;
	uint32_t *header = (uint32_t *)client_buffer;

	*header = SDL_SwapBE32(LIST);
	count = SDLNet_TCP_Send(socket, client_buffer, sizeof(Formulary));
	count = SDLNet_TCP_Recv(socket, client_buffer, BUFFER_SIZE);
	memcpy(publishers, client_buffer+sizeof(Formulary), count-sizeof(Formulary));
	return (count - sizeof(Formulary)) / sizeof(Publisher);
}

char outward_buffer[BUFFER_SIZE], inward_buffer[BUFFER_SIZE];

int mw_check_inbox () {
	int count;
	uint32_t *header = (uint32_t *)client_buffer;

	*header = SDL_SwapBE32(CHECK_INBOX);
	count = SDLNet_TCP_Send(socket, client_buffer, sizeof(Formulary));
	count = SDLNet_TCP_Recv(socket, client_buffer, BUFFER_SIZE);
	memcpy(inward_buffer, client_buffer+sizeof(Formulary), count-sizeof(Formulary));
	return count - sizeof(Formulary);
}

int mw_subscribe (int publisher_id) {
	int count;
	uint32_t *header = (uint32_t *)client_buffer;
	uint32_t *id = (uint32_t *)(client_buffer + sizeof(uint32_t));

	*header = SDL_SwapBE32(SUBSCRIBE);
	*id = SDL_SwapBE32(publisher_id);
	count = SDLNet_TCP_Send(socket, client_buffer, sizeof(Formulary));
	count = SDLNet_TCP_Recv(socket, client_buffer, BUFFER_SIZE);
	memcpy(inward_buffer, client_buffer+sizeof(Formulary), count-sizeof(Formulary));
	return count - sizeof(Formulary);
}

int mw_quit () {
	SDLNet_TCP_Close(socket);
	SDLNet_Quit();
	SDL_Quit(); 

	return 1;
}


/*
static int talk_to_server (void *data) {
	int count, message_length;
	printf("client1\n");
	while (1) {
		message_length = SDL_AtomicGet(&outward_buffer_length);
		if (message_length > 0) {
			count = SDLNet_TCP_Send(socket, outward_buffer, message_length);
			printf("client2\n");
			SDL_AtomicSet(&outward_buffer_length, 0);

			while (SDL_AtomicGet(&inward_buffer_length) > 0) {
				SDL_Delay(30);
			}
			printf("client3\n");
			count = SDLNet_TCP_Recv(socket, inward_buffer, BUFFER_SIZE); 
			SDL_AtomicSet(&inward_buffer_length, count);
			printf("client4\n");
		} else {
			SDL_Delay(30);
		}
	}
	return 1;
}
*/

struct Subscription {
	uint32_t num_publishers;
	uint32_t data_length;
	char publishers[16][2];
	char data[BUFFER_SIZE];
} subscriptions[MAX_SUBSCRIPTIONS];
int num_subscriptions = 0;

typedef struct Follow {
	int subscription, last_length;
} Follow;

	
static int serve_client (void *data) {
	TCPsocket client_socket = data;
	char buffer[BUFFER_SIZE];
	uint32_t *header = (uint32_t *)buffer;
	uint32_t *id = (uint32_t *)(buffer + sizeof(uint32_t));
	Formulary form;
	Follow following[32];
	int size, num_following = 0, count;

	printf("boi1\n");
	size = SDLNet_TCP_Recv(client_socket, buffer, BUFFER_SIZE);
	while (size > 0) {
		*header = SDL_SwapBE32(*header);
		*id = SDL_SwapBE32(*id);
		printf("header = 0x%x\n", *header);
		switch (*header) {

		case BECOME_PUBLISHER:
			printf("become publisher\n");
			following[num_following++] = (Follow){ .subscription = num_subscriptions, .last_length = subscriptions[num_subscriptions].data_length };
			strcpy(subscriptions[num_subscriptions].publishers[0], buffer+sizeof(uint32_t));
			subscriptions[num_subscriptions].num_publishers++;
			form = (Formulary){ .header = OK, .id = num_subscriptions };
			mw_serialize(&form, ">2d");
			SDLNet_TCP_Send(client_socket, &form, sizeof(Formulary));
			num_subscriptions++;
			printf("become publisher\n");
			break;

		case JOIN_PUBLISHER:
			following[num_following++] = (Follow){ .subscription = *id, .last_length = subscriptions[*id].data_length };
			strcpy(subscriptions[*id].publishers[1], buffer+sizeof(Formulary));
			subscriptions[*id].num_publishers++;
			form = (Formulary){ .header = OK, .id = *id };
			mw_serialize(&form, ">2d");
			SDLNet_TCP_Send(client_socket, &form, sizeof(Formulary)); 
			break;

		case PUBLISH:
			memcpy(subscriptions[*id].data + subscriptions[*id].data_length, buffer + sizeof(Formulary), size - sizeof(Formulary));
			subscriptions[*id].data_length += size - sizeof(Formulary);
			for (int i = 0; i < num_following; i++) {
				if (following[i].subscription == *id) {
					following[i].last_length = subscriptions[following[i].subscription].data_length;
					break;
				}
			}
			form = (Formulary){ .header = OK, .id = *id };
			mw_serialize(&form, ">2d");
			SDLNet_TCP_Send(client_socket, &form, sizeof(Formulary)); 
			break;

		case SUBSCRIBE:
			following[num_following++] = (Follow){ .subscription = *id, .last_length = subscriptions[*id].data_length };
			*header = OK;
			memcpy(buffer+sizeof(Formulary), subscriptions[*id].data, subscriptions[*id].data_length);
			mw_serialize(buffer, ">2d");
			SDLNet_TCP_Send(client_socket, buffer, sizeof(Formulary) + subscriptions[*id].data_length); 
			break;

		case CHECK_INBOX:
			for (int i = 0; i < num_following; i++) {
				if (subscriptions[following[i].subscription].data_length > following[i].last_length) {
					memcpy(buffer+2*sizeof(uint32_t), &subscriptions[*id], subscriptions[*id].data_length + 4*sizeof(uint32_t));
					*header = SDL_SwapBE32(OK);
					*id = SDL_SwapBE32(following[i].subscription);
					SDLNet_TCP_Send(client_socket, buffer, sizeof(Formulary) + subscriptions[*id].data_length + 4*sizeof(uint32_t)); 
					following[i].last_length = subscriptions[following[i].subscription].data_length;
					break;
				}
			} 
			break;

		case LIST:
			count = sizeof(Formulary);
			for (int i = 0; i < num_subscriptions; i ++) {
				memcpy(&buffer[count], &subscriptions[i], 40);
				count += 40;
			}
			*header = SDL_SwapBE32(OK);
			SDLNet_TCP_Send(client_socket, buffer, count);
			printf("listed\n");
			break;

		default:
			printf("unknown message header\n");
			break;
		}
		printf("boi_recv1\n");
		size = SDLNet_TCP_Recv(client_socket, buffer, BUFFER_SIZE);
		printf("boi_recv2\n");
	}
	printf("boi3\n"); 
	return 0;
}


int mw_serialize (void *data, const char *fmt) {
	int direction, max_var_size = 1;
	char *start = data;

	if (*fmt == '<') {
		direction = INBOUND;
	} else if (*fmt == '>') {
		direction = OUTBOUND;
	} else set_error("missing starting sign '>', for serialization, or '<', for deserialization, in formatted string");
	fmt++;

	for (int repeat = 1; *fmt; fmt++, repeat++) {
		switch (*fmt) {
		case 'b':
			data += repeat;
			repeat = 0;
			break;

		case 'w':
			data = (void *)(((uintptr_t)data + 0x1) & ~0x1);
			for (uint16_t *ptr; repeat > 0; repeat--, data += 2) {
				ptr = data;
				*ptr = SDL_SwapBE16(*ptr);
			}
			if (max_var_size < 2) max_var_size = 2;
			break;

		case 'd':
			data = (void *)(((uintptr_t)data + 0x3) & ~0x3);
			for (uint32_t *ptr; repeat > 0; repeat--, data += 4) {
				ptr = data;
				*ptr = SDL_SwapBE32(*ptr);
			}
			if (max_var_size < 4) max_var_size = 4;
			break;

		case 'q':
			data = (void *)(((uintptr_t)data + 0x7) & ~0x7);
			for (uint64_t *ptr; repeat > 0; repeat--, data += 8) {
				ptr = data;
				*ptr = SDL_SwapBE64(*ptr);
			}
			if (max_var_size < 8) max_var_size = 8;
			break;

		case 'p':
			data = (void *)(((uintptr_t)data + 0x7) & ~0x7);
			if (direction == OUTBOUND) {
				for (int64_t *ptr; repeat > 0; repeat--, data += 8) {
					ptr = data;
					*ptr = *(uintptr_t *)data - (uintptr_t)data;
					*ptr = SDL_SwapBE64(*ptr);
				}
			} else { // direction == INBOUND
				for (int64_t *ptr; repeat > 0; repeat--, data += 8) {
					ptr = data;
					*ptr = SDL_SwapBE64(*ptr);
					*ptr = *(int64_t *)data + (uintptr_t)data;
				}
			}
			if (max_var_size < 8) max_var_size = 8;
			break;

		default:
			if (*fmt >= '0' && *fmt <= '9') {
				repeat = 0;
				do {
					repeat = 10*repeat + (*fmt - '0');
					fmt++;
				} while (*fmt >= '0' && *fmt <= '9');
				repeat--;
				fmt--;
			} else set_error("invalid character '%c' in formatted string of %sserialization", *fmt, (direction == INBOUND) ? "de" : "");
			break;
		}
	}
	data = (void *)(((uintptr_t)data + (max_var_size-1)) & ~(max_var_size-1));

	return (int)((uintptr_t)data - (uintptr_t)start);
}
