#ifndef TRANSPOSITION_H
#define TRANSPOSITION_H

#include "util.h"
#include "move.h"

#include <iostream>
#include <map>

struct Entry {
	int value;
	Move bestMove;
	int8_t knownHeight, nodeType;
	void setPVNode(int val, int height, Move move) {
		this->bestMove = move;
		this->nodeType = 2;
		this->knownHeight = height;
		this->value = val;
	}
	void setAllNode(int val, int height, Move move) {
		this->bestMove = move;
		this->nodeType = 0;
		this->knownHeight = height;
		this->value = val;
	}
	void setCutNode(int val, int height, Move move) {
		this->bestMove = move;
		this->nodeType = 1;
		this->knownHeight = height;
		this->value = val;
	}
};

template<class State>
class TranspositionTable {
public:
	// try to access state
	virtual Entry* probe(const State &state, uint8_t depth) { return NULL; }
	// try to access and force access to it
	virtual Entry* access(const State &state, uint8_t depth) { return NULL; }
};

template<class State>
class HashTranspositionTable : public TranspositionTable<State> {
public:
	Entry* probe(const State &state, uint8_t depth) {
		int size = 1 << 15;
		if(hash[state.getHash() & (size - 1)] == state.getHash()) {
			//ASSERT(!(state < s[state.getHash() & (size - 1)] || s[state.getHash() & (size - 1)] < state));
			return &table[state.getHash() & (size - 1)];
		} else {
			return NULL;
		}
	}
	Entry* access(const State &state, uint8_t depth) {
		int size = 1 << 15;
		if(hash[state.getHash() & (size - 1)] == state.getHash()) {
			return &table[state.getHash() & (size - 1)];
		} else {
			table[state.getHash() & (size - 1)] = Entry();
			hash[state.getHash() & (size - 1)] = state.getHash();
			//s[state.getHash() & (size - 1)] = state;
			return &table[state.getHash() & (size - 1)];
		}
	}
	Entry table[1 << 15];
	uint64_t hash[1 << 15];
	//State s[1 << 20];
};

#endif