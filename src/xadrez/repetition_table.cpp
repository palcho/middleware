#include "repetition_table.h"
#include "util.h"

void RepetitionTable::insert(uint64_t hash) {
	table[size++] = hash;
}

bool RepetitionTable::find(uint64_t hash) {
	for(int i = (int) size - 2; i >= 0 && i >= (int) size - 60; i -= 2) {
		if(table[i] == hash) {
			return true;
		}
	}
	return false;
}

bool RepetitionTable::erase(uint64_t hash) {
	ASSERT(size > 0 && hash == table[size-1]);
	size--;
	return true;
}