#include "common_evaluator.h"
#include "common_board.h"

#include <cmath>

int CommonEvaluator::evaluate(const CommonBoard &board) {
	return int((round(board.getLazyEvaluation()) + 150) / 10);
}