#ifndef ALPHABETA_H
#define ALPHABETA_H

#include "transposition.h"
#include "move.h"
#include "util.h"
#include "repetition_table.h"
#include "pv_table.h"
#include "common_move_generator.h"
#include "common_evaluator.h"
#include "reader.h"

#include <chrono>
#include <vector>

class AlphaBeta {
public:
	AlphaBeta() {
		hit = goodCut = totalCut = count = ply = 0;
	}
	int debugEvaluate(CommonBoard state, int8_t depth, std::vector<Move> &PV) {
		int ans = evaluate(state, depth, PV, -1);
		std::cout << "percentage of good cuts is " << (double) goodCut / totalCut << ", " << totalCut << " cut nodes" << std::endl;
		std::cout << "percentage of good PV is " << (double) goodPV / totalPV << ", " << totalPV << " PV nodes" << std::endl;
		std::cout << totalAll << " all nodes" << std::endl;
		std::cout << "futility (" << badFut << ", " << goodFut << ")" << std::endl;
		return ans;
		/*initialTime = std::chrono::system_clock::now();
		stopped = false;
		searchTime = -1;
		goodFut = badFut = 0;
		//clock_t t = clock();
		this->count = this->hit = this->goodCut = this->totalCut = this->totalPV = this->goodPV = this->totalAll = 0;
		pvPointer = 0;
		for(int i = 1; i < depth; i++) {
			int score = this->evaluate(state, -1e8, 1e8, i, Move());
			PV = pvtable.getPV(0);
			if(score == 300000 || score == -300000) {
				return score;
			}
			std::cout << "ended depth " << i << ", score is " << score << std::endl;
			for(int j = 0; j < (int) PV.size(); j++) {
				std::cout << PV[j].toString() << ' ';
				pvStack[pvPointer++] = PV[(int) PV.size() - j - 1];
			}std::cout << std::endl;
		}
		int ans = this->evaluate(state, -1e8, 1e8, depth, Move());
		// principal variation
		PV = pvtable.getPV(0);
		//std::cout << hit << " hits, computed " << count << " nodes in " << (double) (clock() - t) / CLOCKS_PER_SEC << "s, average is " << count / ((double) (clock() - t) / CLOCKS_PER_SEC) << " nodes per second\n";
		std::cout << "percentage of good cuts is " << (double) goodCut / totalCut << ", " << totalCut << " cut nodes" << std::endl;
		std::cout << "percentage of good PV is " << (double) goodPV / totalPV << ", " << totalPV << " PV nodes" << std::endl;
		std::cout << totalAll << " all nodes" << std::endl;
		std::cout << "futility (" << badFut << ", " << goodFut << ")" << std::endl;
		return ans;*/
	}

	int evaluate(CommonBoard state, int8_t depth, std::vector<Move> &PV, int timeLimit) {
		initialTime = std::chrono::system_clock::now();
		searchTime = timeLimit;
		stopped = false;
		goodFut = badFut = 0;
		this->count = this->hit = this->goodCut = this->totalCut = this->totalPV = this->goodPV = this->totalAll = 0;
		pvPointer = 0;
		int ans = 0;
		for(int i = 1; i <= depth; i++) {
			int score = this->evaluate(state, -1e8, 1e8, i, Move());
			if(stopped) {
				return ans;
			}
			ans = score;
			PV = pvtable.getPV(0);
			if(score == 300000 || score == -300000) {
				return score;
			}
			int curTime = std::chrono::duration_cast< std::chrono::duration<double> >(std::chrono::system_clock::now() - initialTime).count() * 1000;
			curTime = curTime + 1;
			std::cout << "info depth " << i << " score cp " << score << " time " << curTime << " nodes " << count << " nps " << (count * 1000) / curTime << " pv";
			for(int j = 0; j < (int) PV.size(); j++) {
				std::cout << ' ' << PV[j].toString();
				pvStack[pvPointer++] = PV[(int) PV.size() - j - 1];
			}std::cout << std::endl;
		}
		return ans;
	}
	void clearRepetition() {
		repTable.clear();
	}
	void addPositionHash(uint64_t hash) {
		repTable.insert(hash);
	}
	const int64_t getCount() const { return this->count; }
	const int64_t getHit() const { return this->hit; }
private:
	int evaluate(CommonBoard &state, int alpha, int beta, int8_t depthLeft, Move lastMove);
	int quiescence(CommonBoard &state, int alpha, int beta, int8_t depthLeft);
	Move moves[40][300];
	int movesScore[40][300];
	Move killer[40][2], counterMove[16][64];
	int64_t count, hit, goodCut, totalCut, goodPV, totalPV, totalAll;
	CommonMoveGenerator gen;
	CommonEvaluator eval;
	HashTranspositionTable<CommonBoard> transTable;
	RepetitionTable repTable;
	PVTable pvtable;
	uint8_t pvPointer;
	Move pvStack[40];
	uint8_t ply;
	int goodFut, badFut;
	bool stopped;
	int searchTime;
	void checkStop() {
		if(searchTime != -1) {
			int curTime = std::chrono::duration_cast< std::chrono::duration<double> >(std::chrono::system_clock::now() - initialTime).count() * 1000;
			if(curTime >= searchTime) {
				stopped = true;
			}
		}
		auto line = reader::curLine();
		if(!line.empty()) {
			reader::clear();
			if(line[0] == "stop") {
				stopped = true;
			}
		}
	}
	std::chrono::system_clock::time_point initialTime;
};

#endif