#ifndef COMMON_BOARD_H
#define COMMON_BOARD_H

#include "board.h"
#include "piece_position_table.h"
#include "zobrist.h"
#include "move.h"
#include "util.h"

#include <cstdint>
#include <string>
#include <vector>

class CommonBoard {
public:
	CommonBoard() { ppt = NULL; hashTable = NULL; init(); }
	~CommonBoard() {}
	void init();
	bool loadFEN(const std::string FEN);
	// command stuff
	void flip();
	void makeCommand(Move cmd);
	void undoCommand(Move cmd, uint32_t positionInfo);
	inline const uint32_t getPositionInfo() const { return castle | (enPassant << 4); }
	// piece extraction
	const uint8_t getPieceList(uint8_t piece, uint8_t *pieceArray) const;
	inline const uint8_t getRawPiece(uint8_t row, uint8_t column) const { return getRawPiece((row << 3) | column); }
	inline const uint8_t getRawPiece(uint8_t id) const { return (this->board[id>>1] >> ((id&1)<<2)) & 15; }
	inline const char getPiece(uint8_t row, uint8_t column) const { return piece_decoder[getRawPiece((row << 3) | column)]; }
	const uint8_t getCastle() const { return castle; }
	const uint8_t getEnPassant() const { return enPassant; }
	// lazy evaluation
	const double getLazyEvaluation() const;
	const double calculateLazyEvaluation() const;
	void setPositionTable(PiecePositionTable *table) { ppt = table; }
	// hash evaluation
	const inline uint64_t getHash() const { return hash; }
	const uint64_t calculateHash() const;
	void setZobristTable(Zobrist *table) { hashTable = table; }
	// other
	const uint8_t squareAttacked(uint8_t row, uint8_t column) const;
	const bool inCheck() const;
	const bool operator < (const CommonBoard &other) const;
	inline const bool isWhiteTurn () const { return turn; }
	const void print() const;
private:
	void eraseId(uint8_t id);
	void setId(uint8_t id, uint8_t piece);
	uint8_t board[32];
	uint8_t pieceList[16][11];
	uint8_t castle, enPassant;
	bool turn;
	double lazy;
	PiecePositionTable *ppt;
	Zobrist *hashTable;
	uint64_t hash;
};

/*
	PIECES:

	'.' = empty
	'x' = invalid
	'PNBRQK' = white
	'pnbrqk' = black
	0 = empty
	1 = P
	2 = N
	3 = B
	4 = R
	5 = Q
	6 = K
	9 = p
	10 = n
	11 = b
	12 = r
	13 = q
	14 = k
	4 bits per position
*/

#endif