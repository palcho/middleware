#include <iostream>
#include <stack>
#include <ctime>
#include <cstdlib>
#include <bitset>
#include "common_board.h"
#include "board.h"
#include "common_evaluator.h"
#include "common_move_generator.h"
#include "alphabeta.h"
#include "transposition.h"
#include "piece_position_table.h"
#include "move.h"
#include "perft.h"

void printBoard(CommonBoard &board) {
	std::cout << (board.isWhiteTurn() ? "White to play." : "Black to play.") << " Is " << (board.inCheck() ? "" : "not ") << "in check!\n";
	std::cout << "En passant column is: " << (int) board.getEnPassant() << std::endl;
	std::cout << "castle mask is " << std::bitset<4>(board.getCastle()) << std::endl;
	for(int i = 0; i < 8; i++) {
		for(int j = 0; j < 8; j++) {
			std::cout << board.getPiece(7-i, j);
		}
		std::cout << std::endl;
	}
}


AlphaBeta searcher;


int main() {
	srand(time(0));
	CommonBoard cur;
	PiecePositionTable pieceTable;
	cur.setPositionTable(&pieceTable);
	cur.setZobristTable(new Zobrist);
	cur.init();
	std::stack<std::pair<Move, uint32_t> > st;
	CommonMoveGenerator gen;
	while(1) {
		std::cerr << "New command!" << std::endl;
		std::cout << "Current board:\n";
		printBoard(cur);
		std::cout << "Please input a command!\n";
		std::string cmd;
		std::cin >> cmd;
		if(cmd == "load") {
			std::string FEN;
			for(int i = 0; i < 6; i++) {
				std::string str;
				std::cin >> str;
				if(i) {
					FEN += ' ';
				}
				FEN += str;
			}
			while(!st.empty()) {
				st.pop();
			}
			std::cout << "whole FEN is: " << FEN << std::endl;
			cur.loadFEN(FEN);
		} else if(cmd == "search") {
			int depth;
			std::cin >> depth;
			uint32_t positionState = cur.getPositionInfo();
			std::vector<Move> PV;
			int best = searcher.debugEvaluate(cur, depth, PV);
			std::cout << "best score is " << best << std::endl;
			std::cout << "got PV: "; for(auto move : PV) { std::cout << move.toString() << ' '; } std::cout << std::endl;
			Move bestMove = PV[0];
			cur.makeCommand(bestMove);
			cur.flip();
			st.push({bestMove, positionState});
		} else if(cmd == "gen" || cmd == "generate") {
			Move moves[300];
			int size = gen.generate(cur, moves);
			int positionState = cur.getPositionInfo();
			std::cout << "size is " << size << '\n';
			for(int i = 0; i < size; i++) {
				// applying move
				cur.makeCommand(moves[i]);
				cur.flip();
				printBoard(cur);
				// undoing move
				cur.flip();
				cur.undoCommand(moves[i], positionState);
				//printBoard(cur);
			}
		} else if(cmd == "exit") {
			break;
		} else if(cmd == "flip") {
			cur.flip();
			st.push({Move(0, 0), 0});
		} else if(cmd == "pop") {
			cur.flip();
			cur.undoCommand(st.top().first, st.top().second);
			st.pop();
		} else if(cmd == "perft") {
			int depth;
			std::cin >> depth;
			std::cout << perft(cur, gen, depth) << '\n';
		} else if(cmd == "divide") {
			int depth;
			std::cin >> depth;
			Move moves[300];
			int size = gen.generate(cur, moves);
			auto positionState = cur.getPositionInfo();
			for(int i = 0; i < size; i++) {
				// applying move
				cur.makeCommand(moves[i]);
				if(cur.inCheck()) {
					cur.undoCommand(moves[i], positionState);
					continue;
				}
				cur.flip();
				std::cout << moves[i].toString() << ": " << perft(cur, gen, depth - 1) << std::endl;
				cur.flip();
				cur.undoCommand(moves[i], positionState);
			}
		} else {
			uint8_t fromCol = cmd[0] - 'a', fromRow = cmd[1] - '1', toCol = cmd[2] - 'a', toRow = cmd[3] - '1';
			Move move = gen.generate(cur, fromRow, fromCol, toRow, toCol);
			bool valid = false;
			Move moves[300];
			int size = gen.generate(cur, moves);
			for(int i = 0; i < size; i++) {
				if(move == moves[i]) {
					valid = true;
				}
			}
			if(valid) {
				st.push({move, cur.getPositionInfo()});
				cur.makeCommand(move);
				cur.flip();
			} else {
				std::cout << "Invalid move :)" << std::endl;
			}
		}
	}
}