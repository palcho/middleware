#ifndef PV_TABLE_H
#define PV_TABLE_H

#include "move.h"

#include <vector>

class PVTable {
public:
	std::vector<Move> getPV(int d);
	void insertPV(int d, Move move);
private:
	Move table[50][50];
};

#endif