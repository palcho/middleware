#ifndef PIECE_POSITION_TABLE_H
#define PIECE_POSITION_TABLE_H

#include <string>

class PiecePositionTable {
public:
	PiecePositionTable() { init(); }
	~PiecePositionTable() {}
	void init();
	void init(std::string path);
	inline double getValue(uint8_t piece, uint8_t x, uint8_t y) { return this->table[piece][x][y]; }
	inline double getValue(uint8_t piece, uint8_t id) { return this->table[piece][id>>3][id&7]; }
private:
	double table[16][8][8];
};

#endif