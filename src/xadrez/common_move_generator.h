#ifndef COMMON_MOVE_GENERATOR_H
#define COMMON_MOVE_GENERATOR_H

#include "common_board.h"
#include "move.h"

class CommonMoveGenerator {
public:
	Move generate(const CommonBoard &board, uint8_t fromRow, uint8_t fromCol, uint8_t toRow, uint8_t toCol);
	uint16_t generate(const CommonBoard &board, Move* moves, uint8_t typeMask = 255);
private:
	Move quietMoves[300], captureMoves[300], specialMoves[100];
	uint8_t pieceArray[11];
};

#endif