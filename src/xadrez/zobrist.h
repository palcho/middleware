#ifndef ZOBRIST_H
#define ZOBRIST_H

#include <cstdint>

struct Zobrist {
	Zobrist() { init(); }
	~Zobrist() {}
	void init();
	uint64_t castle[16], piece[16][64], enpassant[9], turn[2];
};

#endif