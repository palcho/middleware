#ifndef PERFT_H
#define PERFT_H

#include "move.h"
#include "util.h"

#include <iostream>
#include <cstdint>

template<class State, class Generator>
uint64_t perft(State &state, Generator &gen, int depth) {
	ASSERT(state.getHash() == state.calculateHash());
	if(depth == 0) {
		return 1;
	}
	uint64_t ans = 0;
	Move moves[300];
	int size = gen.generate(state, moves);
	auto stateInfo = state.getPositionInfo();
	for(int i = 0; i < size; i++) {
		state.makeCommand(moves[i]);
		if(state.inCheck()) {
			state.undoCommand(moves[i], stateInfo);
			continue;
		}
		state.flip();
		ans += perft(state, gen, depth - 1);
		state.flip();
		state.undoCommand(moves[i], stateInfo);
	}
	return ans;
}

#endif