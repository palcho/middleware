#ifndef COMMON_EVALUATOR_H
#define COMMON_EVALUATOR_H

#include "common_board.h"

class CommonEvaluator {
public:
	int evaluate(const CommonBoard &board);
};

#endif