#include "common_move_generator.h"
#include "common_board.h"
#include "move.h"
#include "util.h"
#include "cstring"

Move CommonMoveGenerator::generate(const CommonBoard &board, uint8_t fromRow, uint8_t fromCol, uint8_t toRow, uint8_t toCol) {
	return Move((fromRow<<3)|fromCol, (toRow<<3)|toCol, board.getRawPiece(toRow, toCol) & 7);
}

#define LIST_MOVE_GENERATION

uint16_t CommonMoveGenerator::generate(const CommonBoard &board, Move* moves, uint8_t typeMask) {
	bool valid = false;
	uint16_t quietSize = 0, captureSize = 0, specialSize = 0;
	uint8_t mP = wP + (board.isWhiteTurn() ? 0 : 8);
	uint16_t size;
	#ifndef LIST_MOVE_GENERATION
	uint8_t mK = mP + (wK - wP);
	for(int8_t i = 0; i < 8; i++) for(int8_t j = 0; j < 8; j++) {
		uint8_t piece = board.getRawPiece(i, j);
		if(piece < mP || piece > mK) {
			continue;
		} else if((piece & 7) == wP) {
			// pawn
			int8_t dx = board.isWhiteTurn() ? 1 : -1;
			int8_t x = i, y = j;
			for(uint8_t rep = 0; rep < ((dx & 7) == i ? 2 : 1); rep++) {
				x += dx;
				if(x >= 8 || x < 0 || board.getRawPiece(x, y)) { break; }
				quietMoves[quietSize++] = Move((i<<3) | j, (x<<3) | y);
			}
			// capture
			for(int8_t dy = -1; dy <= 1; dy += 2) {
				x = i + dx, y = j + dy;
				if(x < 0 || x >= 8 || y < 0 || y >= 8) { continue; }
				uint8_t toPiece = board.getRawPiece(x, y);
				if(toPiece && (toPiece ^ piece) >= 8) { captureMoves[captureSize++] = Move((i<<3) | j, (x<<3) | y, toPiece & 7); }
			}
			// MISSING EN PASSANT!
		} else if((piece & 7) == wN) {
			// knight
			int8_t dx = -2, dy = 1;
			for(uint8_t rep = 0; rep < 4; rep++) {
				std::swap(dx, dy);
				dy = -dy;
				int8_t x = i + dx, y = j + dy;
				if(x < 0 || x >= 8 || y < 0 || y >= 8) { continue; }
				uint8_t toPiece = board.getRawPiece(x, y);
				if(toPiece && (toPiece ^ piece) < 8) { continue; }
				if(toPiece) { captureMoves[captureSize++] = Move((i<<3) | j, (x<<3) | y, toPiece & 7); }
				else { quietMoves[quietSize++] = Move((i<<3) | j, (x<<3) | y); }
			}
			dx = -1, dy = 2;
			for(uint8_t rep = 0; rep < 4; rep++) {
				std::swap(dx, dy);
				dy = -dy;
				int8_t x = i + dx, y = j + dy;
				if(x < 0 || x >= 8 || y < 0 || y >= 8) { continue; }
				uint8_t toPiece = board.getRawPiece(x, y);
				if(toPiece && (toPiece ^ piece) < 8) { continue; }
				if(toPiece) { captureMoves[captureSize++] = Move((i<<3) | j, (x<<3) | y, toPiece & 7); }
				else { quietMoves[quietSize++] = Move((i<<3) | j, (x<<3) | y); }
			}
		} else if((piece & 7) == wB) {
			// bishop
			int8_t dx = -1, dy = 1;
			for(uint8_t rep = 0; rep < 4; rep++) {
				std::swap(dx, dy);
				dy = -dy;
				int8_t x = i, y = j;
				while(1) {
					x += dx, y += dy;
					if(x < 0 || x >= 8 || y < 0 || y >= 8) { break; }
					uint8_t toPiece = board.getRawPiece(x, y);
					if(toPiece && (toPiece ^ piece) < 8) { break; }
					if(toPiece) { captureMoves[captureSize++] = Move((i<<3) | j, (x<<3) | y, toPiece & 7); break; }
					else { quietMoves[quietSize++] = Move((i<<3) | j, (x<<3) | y); }
				}
			}
		} else if((piece & 7) == wR) {
			// rook
			int8_t dx = -1, dy = 0;
			for(uint8_t rep = 0; rep < 4; rep++) {
				std::swap(dx, dy);
				dy = -dy;
				int8_t x = i, y = j;
				while(1) {
					x += dx, y += dy;
					if(x < 0 || x >= 8 || y < 0 || y >= 8) { break; }
					uint8_t toPiece = board.getRawPiece(x, y);
					if(toPiece && (toPiece ^ piece) < 8) { break; }
					if(toPiece) { captureMoves[captureSize++] = Move((i<<3) | j, (x<<3) | y, toPiece & 7); break; }
					else { quietMoves[quietSize++] = Move((i<<3) | j, (x<<3) | y); }
				}
			}
		} else if((piece & 7) == wQ) {
			int8_t dx = -1, dy = 1;
			for(uint8_t rep = 0; rep < 4; rep++) {
				std::swap(dx, dy);
				dy = -dy;
				int8_t x = i, y = j;
				while(1) {
					x += dx, y += dy;
					if(x < 0 || x >= 8 || y < 0 || y >= 8) { break; }
					uint8_t toPiece = board.getRawPiece(x, y);
					if(toPiece && (toPiece ^ piece) < 8) { break; }
					if(toPiece) { captureMoves[captureSize++] = Move((i<<3) | j, (x<<3) | y, toPiece & 7); break; }
					else { quietMoves[quietSize++] = Move((i<<3) | j, (x<<3) | y); }
				}
			}
			dx = -1, dy = 0;
			for(uint8_t rep = 0; rep < 4; rep++) {
				std::swap(dx, dy);
				dy = -dy;
				int8_t x = i, y = j;
				while(1) {
					x += dx, y += dy;
					if(x < 0 || x >= 8 || y < 0 || y >= 8) { break; }
					uint8_t toPiece = board.getRawPiece(x, y);
					if(toPiece && (toPiece ^ piece) < 8) { break; }
					if(toPiece) { captureMoves[captureSize++] = Move((i<<3) | j, (x<<3) | y, toPiece & 7); break; }
					else { quietMoves[quietSize++] = Move((i<<3) | j, (x<<3) | y); }
				}
			}
		} else if((piece & 7) == wK) {
			// king
			valid = true;
			for(int8_t dx = -1; dx <= 1; dx++) for(int8_t dy = -1; dy <= 1; dy++) {
				int8_t x = i + dx, y = j + dy;
				if(x < 0 || x >= 8 || y < 0 || y >= 8) { continue; }
				uint8_t toPiece = board.getRawPiece(x, y);
				if(toPiece && (toPiece ^ piece) < 8) { continue; }
				if(toPiece) { captureMoves[captureSize++] = Move((i<<3) | j, (x<<3) | y, toPiece & 7); }
				else { quietMoves[quietSize++] = Move((i<<3) | j, (x<<3) | y); }
			}
		}
	}
	#else
	uint8_t piece = mP;
	// pawn
	size = board.getPieceList(mP + (wP - wP), pieceArray);
	for(uint8_t pos = 0; pos < size; pos++) {
		int8_t i = (pieceArray[pos] >> 3) & 7, j = pieceArray[pos] & 7;
		int8_t dx = board.isWhiteTurn() ? 1 : -1;
		int8_t x = i, y = j;
		for(uint8_t rep = 0; rep < ((dx == 1 ? 1 : 6) == i ? 2 : 1); rep++) {
			x += dx;
			if((x&7) != x || board.getRawPiece(x, y)) { break; }
			if(x == 7 || x == 0) {
				// promotion
				for(int promotedPiece = wP+1; promotedPiece < wK; promotedPiece++) {
					specialMoves[specialSize++] = Move((i<<3) | j, (x<<3) | y, 0, promotedPiece);
				}
			} else {
				quietMoves[quietSize++] = Move((i<<3) | j, (x<<3) | y);
			}
		}
		// capture
		for(int8_t dy = -1; dy <= 1; dy += 2) {
			x = i + dx, y = j + dy;
			if(((x|y)&7) != (x|y)) { continue; }
			uint8_t toPiece = board.getRawPiece(x, y);
			if(toPiece && (toPiece ^ piece) >= 8) {
				if(x == 7 || x == 0) {
					// promotion
					for(int promotedPiece = wP+1; promotedPiece < wK; promotedPiece++) {
						specialMoves[specialSize++] = Move((i<<3) | j, (x<<3) | y, toPiece & 7, promotedPiece);
					}
				} else {
					captureMoves[captureSize++] = Move((i<<3) | j, (x<<3) | y, toPiece & 7);
				}
			}
		}
		// en passant
		if(i == (dx == 1 ? 4 : 3) && board.getEnPassant() < 8 && abs(board.getEnPassant() - j) == 1) {
			//std::cout << "generated en passant!" << std::endl;
			captureMoves[captureSize++] = Move((i<<3) | j, (i<<3) | board.getEnPassant(), wP, 1 << 3);
		}
	}
	// knight
	size = board.getPieceList(mP + (wN - wP), pieceArray);
	for(uint8_t pos = 0; pos < size; pos++) {
		int8_t i = (pieceArray[pos] >> 3) & 7, j = pieceArray[pos] & 7;
		int8_t dx = -2, dy = 1;
		for(uint8_t rep = 0; rep < 4; rep++) {
			std::swap(dx, dy);
			dy = -dy;
			int8_t x = i + dx, y = j + dy;
			if(((x|y)&7) != (x|y)) { continue; }
			uint8_t toPiece = board.getRawPiece(x, y);
			if(toPiece && (toPiece ^ piece) < 8) { continue; }
			if(toPiece) { captureMoves[captureSize++] = Move((i<<3) | j, (x<<3) | y, toPiece & 7); }
			else { quietMoves[quietSize++] = Move((i<<3) | j, (x<<3) | y); }
		}
		dx = -1, dy = 2;
		for(uint8_t rep = 0; rep < 4; rep++) {
			std::swap(dx, dy);
			dy = -dy;
			int8_t x = i + dx, y = j + dy;
			if(((x|y)&7) != (x|y)) { continue; }
			uint8_t toPiece = board.getRawPiece(x, y);
			if(toPiece && (toPiece ^ piece) < 8) { continue; }
			if(toPiece) { captureMoves[captureSize++] = Move((i<<3) | j, (x<<3) | y, toPiece & 7); }
			else { quietMoves[quietSize++] = Move((i<<3) | j, (x<<3) | y); }
		}
	}
	// bishop
	size = board.getPieceList(mP + (wB - wP), pieceArray);
	for(uint8_t pos = 0; pos < size; pos++) {
		int8_t i = (pieceArray[pos] >> 3) & 7, j = pieceArray[pos] & 7;
		int8_t dx = -1, dy = 1;
		for(uint8_t rep = 0; rep < 4; rep++) {
			std::swap(dx, dy);
			dy = -dy;
			int8_t x = i, y = j;
			while(1) {
				x += dx, y += dy;
				if(((x|y)&7) != (x|y)) { break; }
				uint8_t toPiece = board.getRawPiece(x, y);
				if(toPiece && (toPiece ^ piece) < 8) { break; }
				if(toPiece) { captureMoves[captureSize++] = Move((i<<3) | j, (x<<3) | y, toPiece & 7); break; }
				else { quietMoves[quietSize++] = Move((i<<3) | j, (x<<3) | y); }
			}
		}
	}
	// rook
	size = board.getPieceList(mP + (wR - wP), pieceArray);
	for(uint8_t pos = 0; pos < size; pos++) {
		int8_t i = (pieceArray[pos] >> 3) & 7, j = pieceArray[pos] & 7;
		int8_t dx = -1, dy = 0;
		for(uint8_t rep = 0; rep < 4; rep++) {
			std::swap(dx, dy);
			dy = -dy;
			int8_t x = i, y = j;
			while(1) {
				x += dx, y += dy;
				if(((x|y)&7) != (x|y)) { break; }
				uint8_t toPiece = board.getRawPiece(x, y);
				if(toPiece && (toPiece ^ piece) < 8) { break; }
				if(toPiece) { captureMoves[captureSize++] = Move((i<<3) | j, (x<<3) | y, toPiece & 7); break; }
				else { quietMoves[quietSize++] = Move((i<<3) | j, (x<<3) | y); }
			}
		}
	}
	// queen
	size = board.getPieceList(mP + (wQ - wP), pieceArray);
	for(uint8_t pos = 0; pos < size; pos++) {
		int8_t i = (pieceArray[pos] >> 3) & 7, j = pieceArray[pos] & 7;
		int8_t dx = -1, dy = 0;
		for(uint8_t rep = 0; rep < 4; rep++) {
			std::swap(dx, dy);
			dy = -dy;
			int8_t x = i, y = j;
			while(1) {
				x += dx, y += dy;
				if(((x|y)&7) != (x|y)) { break; }
				uint8_t toPiece = board.getRawPiece(x, y);
				if(toPiece && (toPiece ^ piece) < 8) { break; }
				if(toPiece) { captureMoves[captureSize++] = Move((i<<3) | j, (x<<3) | y, toPiece & 7); break; }
				else { quietMoves[quietSize++] = Move((i<<3) | j, (x<<3) | y); }
			}
		}
		dx = -1, dy = 1;
		for(uint8_t rep = 0; rep < 4; rep++) {
			std::swap(dx, dy);
			dy = -dy;
			int8_t x = i, y = j;
			while(1) {
				x += dx, y += dy;
				if(((x|y)&7) != (x|y)) { break; }
				uint8_t toPiece = board.getRawPiece(x, y);
				if(toPiece && (toPiece ^ piece) < 8) { break; }
				if(toPiece) { captureMoves[captureSize++] = Move((i<<3) | j, (x<<3) | y, toPiece & 7); break; }
				else { quietMoves[quietSize++] = Move((i<<3) | j, (x<<3) | y); }
			}
		}
	}
	// king
	size = board.getPieceList(mP + (wK - wP), pieceArray);
	for(uint8_t pos = 0; pos < size; pos++) {
		int8_t i = (pieceArray[pos] >> 3) & 7, j = pieceArray[pos] & 7;
		valid = true;
		for(int8_t dx = -1; dx <= 1; dx++) for(int8_t dy = -1; dy <= 1; dy++) {
			int8_t x = i + dx, y = j + dy;
			if(((x|y)&7) != (x|y)) { continue; }
			uint8_t toPiece = board.getRawPiece(x, y);
			if(toPiece && (toPiece ^ piece) < 8) { continue; }
			if(toPiece) { captureMoves[captureSize++] = Move((i<<3) | j, (x<<3) | y, toPiece & 7); }
			else { quietMoves[quietSize++] = Move((i<<3) | j, (x<<3) | y); }
		}
		if(board.inCheck()) { continue; }
		// short castle
		if((board.getCastle() & (board.isWhiteTurn() ? 8 : 2)) && board.getRawPiece(i, j+1) == 0 && board.getRawPiece(i, j+2) == 0 && (board.squareAttacked(i, j+1) & (board.isWhiteTurn() ? 2 : 1)) == 0 && (board.squareAttacked(i, j+2) & (board.isWhiteTurn() ? 2 : 1)) == 0) {
			ASSERT(board.getRawPiece(i, j) == (board.isWhiteTurn() ? wK : bK));
			ASSERT(board.getRawPiece(i, j+3) == (board.isWhiteTurn() ? wR : bR));
			specialMoves[specialSize++] = Move((i<<3) | j, (i<<3) | (j+2));
		}
		// long castle
		if((board.getCastle() & (board.isWhiteTurn() ? 4 : 1)) && board.getRawPiece(i, j-1) == 0 && board.getRawPiece(i, j-2) == 0 && board.getRawPiece(i, j-3) == 0 && (board.squareAttacked(i, j-1) & (board.isWhiteTurn() ? 2 : 1)) == 0 && (board.squareAttacked(i, j-2) & (board.isWhiteTurn() ? 2 : 1)) == 0) {
			specialMoves[specialSize++] = Move((i<<3) | j, (i<<3) | (j-2));
		}
	}
	#endif

	size = 0;
	if(!valid) { return size; }
	if(typeMask & 1) { memcpy(moves + size, captureMoves, sizeof(Move) * captureSize); size += captureSize; }
	if(typeMask & 2) { memcpy(moves + size, specialMoves, sizeof(Move) * specialSize); size += specialSize; }
	if(typeMask & 4) { memcpy(moves + size, quietMoves, sizeof(Move) * quietSize); size += quietSize; }
	return size;
}