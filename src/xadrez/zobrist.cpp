#include "zobrist.h"

#include <random>
#include <chrono>

void Zobrist::init() {
	// castle
	std::mt19937_64 rng(std::chrono::steady_clock::now().time_since_epoch().count());
	std::uniform_int_distribution<uint64_t> dist(0, ~((uint64_t) 0));
	for(int i = 0; i < 16; i++) {
		this->castle[i] = dist(rng);
	}
	// pieces
	for(int i = 0; i < 16; i++) {
		for(int j = 0; j < 64; j++) {
			this->piece[i][j] = dist(rng);
		}
	}
	// en passant
	for(int i = 0; i < 9; i++) {
		this->enpassant[i] = dist(rng);
	}
	// turn
	for(int i = 0; i < 2; i++) {
		this->turn[i] = dist(rng);
	}
}