#include "alphabeta.h"
#include "transposition.h"
#include "util.h"
#include "move.h"

#include <vector>
#include <iostream>

int AlphaBeta::evaluate(CommonBoard &state, int alpha, int beta, int8_t depthLeft, Move lastMove) {
	ASSERT(alpha < beta);
	if(depthLeft <= 0 || ply == 35) {
		pvtable.insertPV(ply, Move());
		return this->quiescence(state, alpha, beta, 5);
	}
	if(ply > 0 && repTable.find(state.getHash())) {
		pvtable.insertPV(ply, Move());
		return 0;
	}
	bool PVNode = alpha < beta - 1;
	repTable.insert(state.getHash());
	uint32_t stateInfo = state.getPositionInfo();
	Entry* tEntry;
	Move bestMove;
	int best = -1e9;
	uint8_t legal = 0;
	uint8_t gotIncrease = 0;
	//int statScore = eval.evaluate(state);
	if(pvPointer) {
		bestMove = pvStack[--pvPointer];
	} else {
		tEntry = this->transTable.probe(state, depthLeft);
		if(tEntry) {
			this->hit++;
			if(tEntry->knownHeight >= depthLeft) {
				if(tEntry->nodeType == 0) {
					// All node
					if(alpha >= tEntry->value) {
						repTable.erase(state.getHash());
						return tEntry->value;
					}
				} else if(tEntry->nodeType == 1) {
					// Cut node
					if(beta <= tEntry->value) {
						repTable.erase(state.getHash());
						return tEntry->value;
					}
				} else if(tEntry->nodeType == 2) {
					// PV node
					if(tEntry->value <= alpha || tEntry->value >= beta) {
						repTable.erase(state.getHash());
						return tEntry->value;
					}
				}
			}
			bestMove = tEntry->bestMove;
		}
	}
	bool inCheck = state.inCheck();
	if((count & 1023) == 1023) {
		checkStop();
	}
	if(stopped) {
		repTable.erase(state.getHash());
		return 0;
	}
	count++;
	ASSERT(state.getHash() == state.calculateHash());
	ASSERT(abs(state.calculateLazyEvaluation() - state.getLazyEvaluation()) < 1e-9);
	// Null move pruning
	if(!PVNode && depthLeft >= 4 && !lastMove.isNullMove() && !inCheck) {
		state.makeCommand(Move());
		ply++;
		int score = -evaluate(state, -beta, -beta+1, depthLeft - 3, Move());
		ply--;
		state.undoCommand(Move(), stateInfo);
		if(score >= beta) {
			repTable.erase(state.getHash());
			score = evaluate(state, beta-1, beta, depthLeft - 3, Move());
			if(score >= beta) {
				return score;
			}
			repTable.insert(state.getHash());
		}
	}

	// internal iterative deepening
	if(depthLeft >= 8 && bestMove == Move()) {
		evaluate(state, alpha, beta, depthLeft - 7, lastMove);
		tEntry = transTable.probe(state, depthLeft);
		if(tEntry) {
			bestMove = tEntry->bestMove;
		}
	}
	int* mScore = movesScore[ply];
	Move* mList = moves[ply];
	uint16_t size = gen.generate(state, mList);
	{
		// Move ordering
		Move* killerEntry = killer[ply];
		uint8_t piece = state.getRawPiece(lastMove.getTo());
		if(piece == 0) {
			piece = state.isWhiteTurn() ? bP : wP;
		}
		if(lastMove == Move()) {
			piece--;
		}
		Move countermove = counterMove[piece][lastMove.getTo()];
		for(int16_t i = 0; i < size; i++) {
			auto move = mList[i];
			if(move == bestMove) {
				mScore[i] = 1000;
			} else if(move.getPromotedPiece()) {
				mScore[i] = move.getPromotedPiece() + 300;
			} else if(move.isCapture()) {
				// Most valuable victim, least valuable attacker
				mScore[i] = move.getPieceCaptured() * 10 + 9 - (state.getRawPiece(move.getFrom()) & 7);
			} else {
				ASSERT(move.isQuiet());
				mScore[i] = 0;
				if(killerEntry[0] == move) {
					mScore[i] += 12;
				} else if(killerEntry[1] == move) {
					mScore[i] += 11;
				} else if(countermove == move) {
					mScore[i] += 10;
				} else if(ply >= 2 && move == killer[ply-2][0]) {
					mScore[i] += 2;
				} else if(ply >= 2 && move == killer[ply-2][1]) {
					mScore[i] += 1;
				}
			}
		}
	}
	for(uint16_t i = 0; i < size; i++) {
		for(int j = i; j < size; j++) {
			if(mScore[j] > mScore[i]) {
				std::swap(mScore[j], mScore[i]);
				std::swap(mList[j], mList[i]);
			}
		}
		// applying move
		state.makeCommand(mList[i]);
		if(state.inCheck()) {
			state.undoCommand(mList[i], stateInfo);
			continue;
		}
		legal++;
		state.flip();
		bool nextCheck = state.inCheck();
		int8_t extension = 0;
		// check extension
		if(nextCheck) {
			extension = 1;
		}
		int8_t newDepth = depthLeft - 1 + extension;
		// Futility pruning
		/*if(!PVNode && depthLeft <= 3 && statScore + 70 + 130 * depthLeft <= alpha && !inCheck && !nextCheck && mList[i].isQuiet() && mScore[i] == 0) {
			state.flip();
			state.undoCommand(mList[i], stateInfo);
			goodFut++;
			continue;
		}*/
		int score = alpha;
		bool doFullSearch = true;
		// Late move reduction
		if(!PVNode && depthLeft >= 4 && !nextCheck && !inCheck && mList[i].isQuiet() && legal > 3) {
			ply++;
			score = -this->evaluate(state, -(alpha+1), -alpha, newDepth - 2, mList[i]);
			ply--;
			if(score <= alpha) {
				doFullSearch = false;
			}
		}
		if(doFullSearch) {
			ply++;
			if(best == alpha) {
				score = -this->evaluate(state, -(alpha+1), -alpha, newDepth, mList[i]);
				if(score > alpha && score < beta) {
					score = -this->evaluate(state, -beta, -alpha, newDepth, mList[i]);
				}
			} else {
				score = -this->evaluate(state, -beta, -alpha, newDepth, mList[i]);
			}
			ply--;
		}
		// undoing move
		state.flip();
		state.undoCommand(mList[i], stateInfo);
		if(stopped) {
			repTable.erase(state.getHash());
			return 0;
		}
		if(score >= beta) {
			tEntry = this->transTable.access(state, depthLeft);
			totalCut++;
			if(legal == 1) {
				goodCut++;
			}
			if(tEntry) {
				tEntry->setCutNode(score, depthLeft, mList[i]);
			}
			if(mList[i].isQuiet()) {
				// updating killer move
				Move* killerEntry = killer[ply];
				if(!(killerEntry[0] == mList[i])) {
					killerEntry[1] = killerEntry[0];
					killerEntry[0] = mList[i];
				}
				// updating countermove
				uint8_t piece = state.getRawPiece(lastMove.getTo());
				if(piece == 0) {
					piece = state.isWhiteTurn() ? bP : wP;
				}
				if(lastMove == Move()) {
					piece--;
				}
				counterMove[piece][lastMove.getTo()] = mList[i];
			}
			repTable.erase(state.getHash());
			return score;
		}
		if(score > best) {
			best = score;
			bestMove = mList[i];
		}
		if(score > alpha) {
			gotIncrease++;
			alpha = score;
			pvtable.insertPV(ply, bestMove);
		}
	}
	if(stopped) {
		repTable.erase(state.getHash());
		return 0;
	}
	repTable.erase(state.getHash());
	if(legal == 0) {
		pvtable.insertPV(ply, Move());
		return state.inCheck() ? -300000 : 0;
	}
	if(gotIncrease) {
		if(gotIncrease == 1) {
			goodPV++;
		}
		totalPV++;
		tEntry = this->transTable.access(state, depthLeft);
		if(tEntry) { tEntry->setPVNode(alpha, depthLeft, bestMove); }
	} else {
		totalAll++;
		tEntry = this->transTable.access(state, depthLeft);
		if(tEntry) { tEntry->setAllNode(alpha, depthLeft, bestMove); }
	}
	return best;
}

int AlphaBeta::quiescence(CommonBoard &state, int alpha, int beta, int8_t depthLeft) {
	ASSERT(alpha < beta);
	if(stopped) {
		return 0;
	}
	if((count & 1023) == 1023) {
		checkStop();
	}
	count++;
	int best = this->eval.evaluate(state);
	if(best >= beta) { return best; }
	if(alpha < best) { alpha = best; }
	if(ply >= 35 || depthLeft <= 0) { return alpha; }
	uint32_t stateInfo = state.getPositionInfo();
	Move* mList = moves[ply];
	int* mScore = movesScore[ply];
	uint16_t size = gen.generate(state, mList, 3);
	uint16_t legal = 0;
	{
		// Move ordering
		for(int16_t i = 0; i < size; i++) {
			auto move = mList[i];
			if(move.getPromotedPiece()) {
				mScore[i] = move.getPromotedPiece() * 1000;
			} else if(move.isCapture()) {
				// Most valuable victim, least valuable attacker
				mScore[i] = move.getPieceCaptured() * 10 + 9 - (state.getRawPiece(move.getFrom()) & 7);
			} else {
				ASSERT(move.isQuiet());
				mScore[i] = 0;
			}
		}
	}
	for(uint16_t i = 0; i < size; i++) {
		// applying move
		for(int j = i; j < size; j++) {
			if(mScore[j] > mScore[i]) {
				std::swap(mScore[j], mScore[i]);
				std::swap(mList[j], mList[i]);
			}
		}
		state.makeCommand(mList[i]);
		if(state.inCheck()) {
			state.undoCommand(mList[i], stateInfo);
			continue;
		}
		state.flip();
		ply++;
		int score = -this->quiescence(state, -beta, -alpha, depthLeft - 1);
		ply--;
		// undoing move
		state.flip();
		state.undoCommand(mList[i], stateInfo);
		if(stopped) {
			return 0;
		}
		legal++;
		if(score >= beta) {
			return score;
		}
		if(score > best) {
			best = score;
		}
		if(score > alpha) {
			alpha = score;
		}
	}
	return best;
}