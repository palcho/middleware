#include "pv_table.h"
#include "move.h"

#include <vector>
#include <cstring>
#include <iostream>

std::vector<Move> PVTable::getPV(int d) {
	std::vector<Move> ans;
	for(int i = 0; i < 50 && !(table[d][i] == Move()); i++) {
		ans.push_back(table[d][i]);
	}
	return ans;
}

void PVTable::insertPV(int d, Move move) {
	table[d][0] = move;
	if(!(move == Move())) {
		memcpy(table[d] + 1, table[d+1], sizeof(Move) * (40));
	}
}