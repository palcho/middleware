#include "common_board.h"
#include "piece_position_table.h"
#include "util.h"
#include "move.h"

#include <vector>

const char first_row[8] = {wR, wN, wB, wQ, wK, wB, wN, wR};

inline void insertIntoList(uint8_t *list, uint8_t id);
inline void deleteFromList(uint8_t *list, uint8_t id);

void CommonBoard::init() {
	loadFEN("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1");
}

bool CommonBoard::loadFEN(const std::string FEN) {
	for(int i = 0; i < 16; i++) {
		for(int j = 0; j < 11; j++) {
			this->pieceList[i][j] = 0xff;
		}
	}
	for(int j = 0; j < 32; j++) {
		this->board[j] = 0;
	}
	int l = 0, r = 0;
	// load board
	while(r < (int) FEN.size() && FEN[r] != ' ') {
		r++;
	}
	int row = 7, col = 0;
	for(int i = l; i < r; i++) {
		char c = FEN[i];
		if('0' <= c && c <= '9') {
			col += c - '0';
		} else if(c == '/') {
			col = 0;
			row--;
		} else {
			uint8_t side = 'a' <= c && c <= 'z' ? 8 : 0;
			if(side > 0) {
				// black piece
				c = c - 'a' + 'A';
			}
			if(col >= 8 || row < 0) {
				return false;
			}
			int id = (row << 3) | col;
			if(c == 'P') {
				setId(id, wP | side);
			} else if(c == 'N') {
				setId(id, wN | side);
			} else if(c == 'B') {
				setId(id, wB | side);
			} else if(c == 'R') {
				setId(id, wR | side);
			} else if(c == 'Q') {
				setId(id, wQ | side);
			} else if(c == 'K') {
				setId(id, wK | side);
			} else {
				return false;
			}
			col++;
		}
	}
	if(row != 0 || col != 8) {
		return false;
	}
	r = r + 1;
	l = r;
	// side to move
	if(l >= (int) FEN.size()) {
		return false;
	}
	while(r < (int) FEN.size() && FEN[r] != ' ') {
		r++;
	}
	turn = FEN[l] == 'w';
	r = r + 1;
	l = r;
	// castle stuff
	if(l >= (int) FEN.size()) {
		return false;
	}
	while(r < (int) FEN.size() && FEN[r] != ' ') {
		r++;
	}
	castle = 0;
	for(int i = l; i < r; i++) {
		char c = FEN[i];
		if(c == 'K') {
			// white kingside (short)
			castle |= 8;
		} else if(c == 'k') {
			// black kingside (short)
			castle |= 2;
		} else if(c == 'Q') {
			// white queenside (long)
			castle |= 4;
		} else if(c == 'q') {
			// black queenside (long)
			castle |= 1;
		}
	}
	r = r + 1;
	l = r;
	// enpassant
	if(l >= (int) FEN.size()) {
		return false;
	}
	while(r < (int) FEN.size() && FEN[r] != ' ') {
		r++;
	}
	enPassant = 8;
	if('a' <= FEN[l] && FEN[l] <= 'h') {
		enPassant = FEN[l] - 'a';
	}
	// halfclock move
	// fullmove counter
	// setting values
	this->lazy = this->calculateLazyEvaluation();
	this->hash = this->calculateHash();
	if(!this->isWhiteTurn()) {
		lazy = -lazy;
	}
	return true;
}

// command stuff

void CommonBoard::flip() {
	this->turn = !this->turn;
	if(this->hashTable) { this->hash ^= this->hashTable->turn[0] ^ this->hashTable->turn[1]; }
}

void CommonBoard::makeCommand(Move cmd) {
	if(cmd.isNullMove()) {
		if(this->hashTable) { this->hash ^= this->hashTable->enpassant[enPassant] ^ this->hashTable->enpassant[8]; }
		this->enPassant = 8;
		flip();
		return;
	}
	ASSERT(getRawPiece(cmd.getFrom()));
	ASSERT((getRawPiece(cmd.getFrom()) >> 3) != turn);
	if(cmd.isCapture() && (getRawPiece(cmd.getFrom()) & 7) == wP && (cmd.getFrom() >> 3) == (cmd.getTo() >> 3)) {
		// en passant
		if(this->hashTable) { this->hash ^= this->hashTable->enpassant[enPassant] ^ this->hashTable->enpassant[8]; }
		ASSERT(getRawPiece(cmd.getFrom()) % 8 == wP);
		ASSERT(enPassant == cmd.getTo() % 8);
		ASSERT(cmd.getFrom() / 8 == (isWhiteTurn() ? 4 : 3));
		uint8_t toId = cmd.getTo() + (isWhiteTurn() ? 8 : -8);
		ASSERT(getRawPiece(toId) == 0);
		eraseId(cmd.getTo());
		setId(toId, getRawPiece(cmd.getFrom()));
		eraseId(cmd.getFrom());
		enPassant = 8;
		return;
	} else if((getRawPiece(cmd.getFrom()) & 7) == wK && abs((cmd.getFrom() & (int) 7) - (cmd.getTo() & 7)) == 2) {
		// castle
		if(this->hashTable) { this->hash ^= this->hashTable->enpassant[enPassant] ^ this->hashTable->enpassant[8] ^ this->hashTable->castle[castle] ^ this->hashTable->castle[castle & (turn ? 3 : 12)]; }
		if((cmd.getTo() & 7) == 6) {
			// short castle
			ASSERT(getCastle() & (turn ? 8 : 2));
			ASSERT((getRawPiece(cmd.getFrom()) & 7) == wK);
			ASSERT((getRawPiece(cmd.getFrom() + 3) & 7) == wR);
			setId(cmd.getTo(), turn ? wK : bK);
			setId(cmd.getTo() - 1, turn ? wR : bR);
			eraseId(cmd.getTo() - 2);
			eraseId(cmd.getTo() + 1);
		} else {
			// long caste
			setId(cmd.getTo(), turn ? wK : bK);
			setId(cmd.getTo() + 1, turn ? wR : bR);
			eraseId(cmd.getTo() + 2);
			eraseId(cmd.getTo() - 2);
		}
		castle = castle & (turn ? 3 : 12);
		enPassant = 8;
		return;
	}
	if(this->hashTable) { this->hash ^= this->hashTable->enpassant[enPassant] ^ this->hashTable->castle[castle]; }
	if((getRawPiece(cmd.getFrom()) & 7) == wP && abs((cmd.getFrom() >> 3) - (cmd.getTo() >> 3)) == 2) {
		// set en passant
		enPassant = cmd.getFrom() & 7;
	} else {
		enPassant = 8;
	}
	if((getRawPiece(cmd.getFrom()) & 7) == wK) {
		castle = castle & (turn ? 3 : 12);
	}
	// might be able to preprocess this to optimize
	if(cmd.getFrom() == 0 || cmd.getTo() == 0) {
		castle &= 11;
	}
	if(cmd.getFrom() == 7 || cmd.getTo() == 7) {
		castle &= 7;
	}
	if(cmd.getFrom() == 56 || cmd.getTo() == 56) {
		castle &= 14;
	}
	if(cmd.getFrom() == 63 || cmd.getTo() == 63) {
		castle &= 13;
	}
	if(this->hashTable) { this->hash ^= this->hashTable->enpassant[enPassant] ^ this->hashTable->castle[castle]; }
	if(cmd.isCapture()) {
		ASSERT((getRawPiece(cmd.getTo()) & 7) == cmd.getPieceCaptured());
		ASSERT((getRawPiece(cmd.getTo()) >> 3) == turn);
		eraseId(cmd.getTo());
	} else {
		ASSERT(getRawPiece(cmd.getTo()) == 0);
	}
	if(!cmd.getPromotedPiece()) {
		setId(cmd.getTo(), getRawPiece(cmd.getFrom()));
	} else {
		setId(cmd.getTo(), cmd.getPromotedPiece() | (turn ? 0 : 8));
	}
	eraseId(cmd.getFrom());
}

void CommonBoard::undoCommand(Move cmd, uint32_t positionInfo) {
	if(this->hashTable) {
		this->hash ^= this->hashTable->castle[this->castle];
		this->hash ^= this->hashTable->enpassant[this->enPassant];
	}
	this->castle = positionInfo & 0x0f;
	this->enPassant = (positionInfo >> 4) & 0x0f;
	if(this->hashTable) {
		this->hash ^= this->hashTable->castle[this->castle];
		this->hash ^= this->hashTable->enpassant[this->enPassant];
	}
	if(cmd.isNullMove()) { flip(); return; };
	if(getRawPiece(cmd.getTo()) == 0) {
		// en passant
		uint8_t toId = cmd.getTo() + (isWhiteTurn() ? 8 : -8);
		ASSERT(cmd.isCapture() && (getRawPiece(toId) & 7) == wP && (cmd.getFrom() >> 3) == (cmd.getTo() >> 3));
		ASSERT(getRawPiece(toId) % 8 == wP);
		ASSERT(enPassant == cmd.getTo() % 8);
		ASSERT(cmd.getFrom() / 8 == (isWhiteTurn() ? 4 : 3));
		eraseId(toId);
		setId(cmd.getFrom(), turn ? wP : bP);
		setId(cmd.getTo(), turn ? bP : wP);
		return;
	} else if((getRawPiece(cmd.getTo()) & 7) == wK && abs((cmd.getFrom() & 7) - (cmd.getTo() & 7)) == 2) {
		// castle
		if((cmd.getTo() & 7) == 6) {
			// short castle
			setId(cmd.getTo() - 2, turn ? wK : bK);
			setId(cmd.getTo() + 1, turn ? wR : bR);
			eraseId(cmd.getTo());
			eraseId(cmd.getTo() - 1);
		} else {
			// long caste
			setId(cmd.getTo() + 2, turn ? wK : bK);
			setId(cmd.getTo() - 2, turn ? wR : bR);
			eraseId(cmd.getTo());
			eraseId(cmd.getTo() + 1);
		}
		return;
	}
	ASSERT(getRawPiece(cmd.getTo()));
	ASSERT((getRawPiece(cmd.getTo()) >> 3) != turn);
	if(!cmd.getPromotedPiece()) {
		setId(cmd.getFrom(), getRawPiece(cmd.getTo()));
	} else {
		setId(cmd.getFrom(), turn ? wP : bP);
	}
	eraseId(cmd.getTo());
	if(cmd.isCapture()) {
		setId(cmd.getTo(), cmd.getPieceCaptured() | (this->turn ? 8 : 0));
	}
}

inline void CommonBoard::eraseId(uint8_t id) {
	ASSERT(this->getRawPiece(id) != 0);
	deleteFromList(pieceList[getRawPiece(id)], id);
	if(this->ppt) { this->lazy -= this->ppt->getValue(getRawPiece(id), id); }
	if(this->hashTable) { this->hash ^= this->hashTable->piece[getRawPiece(id)][id] ^ this->hashTable->piece[0][id]; }
	this->board[id>>1] &= uint8_t(15 << (((id^1)&1)<<2));
}

inline void CommonBoard::setId(uint8_t id, uint8_t piece) {
	ASSERT(this->getRawPiece(id) == 0);
	insertIntoList(pieceList[piece], id);
	if(this->ppt) { this->lazy += this->ppt->getValue(piece, id); }
	if(this->hashTable) { this->hash ^= this->hashTable->piece[piece][id] ^ this->hashTable->piece[0][id]; }
	this->board[id>>1] |= uint8_t(piece << ((id&1)<<2));
}

// piece extraction

inline void insertIntoList(uint8_t *list, uint8_t id) {
	while(*list != 0xff) list++;
	*list = id;
}

inline void deleteFromList(uint8_t *list, uint8_t id) {
	while(*list != id) list++;
	uint8_t *now = list;
	while(*list != 0xff) list++;
	*now = *(list - 1);
	*(list - 1) = 0xff;
}

const uint8_t CommonBoard::getPieceList(uint8_t piece, uint8_t *pieceArray) const {
	uint8_t size = 0;
	while(pieceList[piece][size] != 0xff) {
		*(pieceArray + size) = pieceList[piece][size];
		size++;
	}
	return size;
}

// lazy evaluation

const double CommonBoard::getLazyEvaluation() const {
	return this->turn ? this->lazy : -this->lazy;
}

const double CommonBoard::calculateLazyEvaluation() const {
	double ans = 0;
	for(uint8_t i = 0; i < 8; i++) for(uint8_t j = 0; j < 8; j++) {
		if(this->ppt) { ans += this->ppt->getValue(getRawPiece(i, j), i, j); }
	}
	return this->turn ? ans : -ans;
}

// hash

const uint64_t CommonBoard::calculateHash() const {
	uint64_t ans = 0;
	if(this->hashTable) {
		for(uint8_t i = 0; i < 64; i++) {
			ans ^= this->hashTable->piece[getRawPiece(i)][i];
		}
		ans ^= this->hashTable->castle[castle];
		ans ^= this->hashTable->turn[this->turn ? 1 : 0];
		ans ^= this->hashTable->enpassant[enPassant];
	}
	return ans;
}

// other

const bool CommonBoard::operator < (const CommonBoard &other) const {
	if(this->getCastle() != other.getCastle()) {
		return this->getCastle() < other.getCastle();
	}
	if(this->getEnPassant() != other.getEnPassant()) {
		return this->getEnPassant() < other.getEnPassant();
	}
	for(uint8_t i = 0; i < 8; i++) for(uint8_t j = 0; j < 8; j++) {
		if(this->getPiece(i, j) != other.getPiece(i, j)) {
			return this->getPiece(i, j) < other.getPiece(i, j);
		}
	}
	return false;
}

const bool CommonBoard::inCheck() const {
	ASSERT(pieceList[wK][0] != 0xff && pieceList[bK][0] != 0xff);
	return this->turn ? (squareAttacked(pieceList[wK][0]>>3, pieceList[wK][0]&7)&2) : (squareAttacked(pieceList[bK][0]>>3, pieceList[bK][0]&7)&1);
}

const uint8_t CommonBoard::squareAttacked(uint8_t row, uint8_t column) const {
	if(row < 0 || row >= 8 || column < 0 || column >= 8) return 3;
	uint8_t ans = 0;
	//if(getRawPiece(row, column)) { ans |= 1<<(getRawPiece(row, column)>>3); }
	int8_t x, y, dx, dy;
	// rook
	dx = -1, dy = 0;
	for(uint8_t rep = 0; rep < 4; rep++) {
		std::swap(dx, dy);
		dy = -dy;
		x = row + dx, y = column + dy;
		bool canKing = true;
		while(0 <= x && x < 8 && 0 <= y && y < 8) {
			if(getRawPiece(x, y)) {
				auto piece = getRawPiece(x, y);
				if(piece == wR || piece == wQ || (canKing && piece == wK)) {
					ans |= 1;
				} else if(piece == bR || piece == bQ || (canKing && piece == bK)) {
					ans |= 2;
				}
				break;
			}
			canKing = false;
			x += dx, y += dy;
		}
	}
	// bishop
	dx = -1, dy = 1;
	for(uint8_t rep = 0; rep < 4; rep++) {
		std::swap(dx, dy);
		dy = -dy;
		x = row + dx, y = column + dy;
		bool canKing = true;
		while(0 <= x && x < 8 && 0 <= y && y < 8) {
			if(getRawPiece(x, y)) {
				auto piece = getRawPiece(x, y);
				if(piece == wB || piece == wQ || (canKing && piece == wK) || (canKing && dx == -1 && piece == wP)) {
					ans |= 1;
				} else if(piece == bB || piece == bQ || (canKing && piece == bK) || (canKing && dx == 1 && piece == bP)) {
					ans |= 2;
				}
				break;
			}
			canKing = false;
			x += dx, y += dy;
		}
	}
	// knight
	dx = -2, dy = 1;
	for(uint8_t rep = 0; rep < 4; rep++) {
		std::swap(dx, dy);
		dy = -dy;
		x = row + dx, y = column + dy;
		if(x < 0 || x >= 8 || y < 0 || y >= 8) {
			continue;
		}
		if(getRawPiece(x, y) == wN) {
			ans |= 1;
		} else if(getRawPiece(x, y) == bN) {
			ans |= 2;
		}
	}
	dx = -1, dy = 2;
	for(uint8_t rep = 0; rep < 4; rep++) {
		std::swap(dx, dy);
		dy = -dy;
		x = row + dx, y = column + dy;
		if(x < 0 || x >= 8 || y < 0 || y >= 8) {
			continue;
		}
		if(getRawPiece(x, y) == wN) {
			ans |= 1;
		} else if(getRawPiece(x, y) == bN) {
			ans |= 2;
		}
	}
	return ans;
}

const void CommonBoard::print() const {
	return;
}