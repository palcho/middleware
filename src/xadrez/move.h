#ifndef MOVE_H
#define MOVE_H

#include "util.h"

#include <cstdint>
#include <string>

class Move {
public:
	Move() { move = 0; }
	Move(int from, int to, int capturedPiece = 0, int promotedPiece = 0) { move = (from) | (to << 6) | (capturedPiece << 12) | (promotedPiece << 15); }
	inline const uint8_t getFrom() const { return move & 0x3f; }
	inline const uint8_t getTo() const { return (move >> 6) & 0x3f; }
	inline const uint8_t getPieceCaptured() const { return (move >> 12) & 0x07; }
	inline const uint8_t getPromotedPiece() const { return (move >> 15) & 0x07; }
	inline const bool isCapture() const { return (move >> 12) & 0x07; }
	inline const bool isQuiet() const { return (move & 0x0fff) == move; }
	inline const bool isNullMove() const { return move == 0; }
	inline const bool isEnPassant() const { return (move >> 18) & 1; }
	inline const bool operator == (const Move &o) const { return move == o.move; }
	inline const std::string toString() const {
		std::string ans = std::string(1, 'a' + getFrom() % 8) + std::string(1, '1' + getFrom() / 8) + std::string(1, 'a' + getTo() % 8) + std::string(1, '1' + getTo() / 8);
		if(getPromotedPiece()) {
			if(getPromotedPiece() == wP) {
				ans += 'p';
			} else if(getPromotedPiece() == wN) {
				ans += 'n';
			} else if(getPromotedPiece() == wB) {
				ans += 'b';
			} else if(getPromotedPiece() == wR) {
				ans += 'r';
			} else if(getPromotedPiece() == wQ) {
				ans += 'q';
			} else {
				ASSERT(0);
			}
		}
		if(isEnPassant()) {
			ans[3] = ans[3] == '5' ? '6' : '3';
		}
		return ans;
	}
private:
	uint32_t move;
};

#endif