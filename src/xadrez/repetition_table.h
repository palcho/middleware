#ifndef REPETITION_TABLE_H
#define REPETITION_TABLE_H

#include <cstdint>

class RepetitionTable {
public:
	RepetitionTable() { size = 0; }
	void insert(uint64_t hash);
	bool erase(uint64_t hash);
	bool find(uint64_t hash);
	void clear() { size = 0; }
private:
	uint64_t table[300];
	int32_t size;
};

#endif