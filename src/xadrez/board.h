#ifndef BOARD_H
#define BOARD_H

#include "move.h"

#include <string>
#include <vector>

class Board {
public:
	// set initial position
	virtual void init() = 0;
	// load PGN
	//virtual void loadPGN(std::string PGN);
	// command stuff
	virtual void flip() = 0;
	virtual void makeCommand(Move move) = 0;
	virtual void undoCommand(Move move, uint32_t positionInfo) = 0;
	const virtual uint32_t getPositionInfo() const;
	// piece extraction
	const virtual uint8_t getPieceList(uint8_t piece, uint8_t *pieceArray) const = 0;
	const virtual uint8_t getRawPiece(uint8_t row, uint8_t column) const = 0;
	const virtual char getPiece(uint8_t row, uint8_t column) const = 0;
	// bitmask of castling rules
	const virtual uint8_t getCastle() const = 0;
	// lazy evaluation
	const virtual double getLazyEvaluation() const { return 0; }
};

/*
	PIECES:

	'.' = empty
	'x' = invalid
	'PNBRQK' = white
	'pnbrqk' = black
	0 = empty
	1 = P
	2 = N
	3 = B
	4 = R
	5 = Q
	6 = K
	9 = p
	10 = n
	11 = b
	12 = r
	13 = q
	14 = k
	4 bits per position
*/

#endif