#ifndef UTIL_H
#define UTIL_H

//#define DEBUG

#ifndef DEBUG
#define ASSERT(x)
#else
#include <cassert>
#define ASSERT(x) assert(x)
#endif

#define EMPTY_SQUARE 0
#define wP 1
#define wN 2
#define wB 3
#define wR 4
#define wQ 5
#define wK 6
#define bP 9
#define bN 10
#define bB 11
#define bR 12
#define bQ 13
#define bK 14

const char piece_decoder[16] = {'.', 'P', 'N', 'B', 'R', 'Q', 'K', '.', '.', 'p', 'n', 'b', 'r', 'q', 'k', '.'};

#endif