ifeq ($(shell uname -o),Msys)
	EXE = bin/middleware.exe
	DBG = bin/mw_db.exe
	LFLAGS = -lmingw32 -lSDL2main
else
	EXE = bin/middleware
	DBG = bin/mw_db
endif


SOURCE = $(addprefix src/, middleware.c)
HEADER = $(addprefix include/, middleware.h)
CFLAGS = -flto -Wall -Wno-parentheses -Wno-missing-braces
IFLAGS = -Iinclude -Llib
LFLAGS += -lm -lSDL2 -lSDL2_net -Wl,-rpath,bin
RFLAGS = -Os
GFLAGS = -ggdb
.PHONY: clean


release: $(EXE)
debug: $(DBG)
all: $(EXE) $(DBG)


$(EXE): $(SOURCE) $(HEADER) makefile
	gcc $(SOURCE) $(RFLAGS) $(CFLAGS) $(IFLAGS) $(LFLAGS) -o $@

$(DBG): $(SOURCE) $(HEADER) makefile
	gcc $(SOURCE) $(GFLAGS) $(CFLAGS) $(IFLAGS) $(LFLAGS) -o $@

clean:
	-rm -f $(EXE) $(DBG)
